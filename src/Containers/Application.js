import React, {Component} from 'react';
import {FlatList, StyleSheet, View} from "react-native";
import {connect} from "react-redux";
import {getData, loadMore} from "../../store/action";
import PlaceItem from "../components/PlaceItem";

class Application extends Component {

  componentDidMount() {
    this.props.getData();
  };

  render() {
    return(
      <View style={styles.container}>
        <FlatList
          data={this.props.data}
          keyExtractor={(item) => item.data.id}
          onEndReached={this.props.loadMore}
          onEndReachedThreshold={0.1}
          renderItem={info => (
            <PlaceItem placeName={info.item.data.title}
                       placeImage={info.item.data.thumbnail}/>
          )}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

const mapStateToProps = state => {
  return {
    data: state.data,
  }
};

const mapDispatchToProps = dispatch => {
  return {
    getData: () => dispatch(getData()),
    loadMore: () => dispatch(loadMore()),
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Application);