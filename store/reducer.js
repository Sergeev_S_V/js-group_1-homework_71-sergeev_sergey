import * as actionTypes from './actionTypes';

const initialState = {
  data: [],
  afterKey: null,
  loading: false,
  error: null,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.REQUEST:
      return {...state, loading: true};
    case actionTypes.SUCCESS_RESPONSE:
      return {...state, data: state.data.concat(action.data), afterKey: action.afterKey, loading: false};
    case actionTypes.ERROR_RESPONSE:
      return {...state, error: action.error, loading: false};
    default:
      return state;
  }
};

export default reducer;