import * as actionTypes from './actionTypes';
import axios from "axios/index";

const baseUrl = 'https://www.reddit.com/r/pics.json';

export const getData = () => dispatch => {
  dispatch(request());
  axios.get(baseUrl)
    .then(resp => resp
      ? dispatch(successResponse(resp.data.data.children, resp.data.data.after))
      : null)
    .catch(error => dispatch(errorResponse(error)));
};

export const loadMore = () => (dispatch, getState) => {
  dispatch(request());
  axios.get(`${baseUrl}?count=25&after=${getState().afterKey}`)
    .then(resp => resp
      ? dispatch(successResponse(resp.data.data.children, resp.data.data.after))
      : null)
    .catch(error => dispatch(errorResponse(error)));
};

export const request = () => {
  return {type: actionTypes.REQUEST};
};

export const successResponse = (data, afterKey) => {
  return {type: actionTypes.SUCCESS_RESPONSE, data, afterKey}
};

export const errorResponse = error => {
  return {type: actionTypes.ERROR_RESPONSE, error}
};